import {ADD_TO_CART, CLEAR_TOTAL_PRICE, FETCH_POST_SUCCESS, REMOVE_FROM_CART} from "./actions";

const initialState = {
  dishes: [],
  order: {},
  totalPrice: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POST_SUCCESS:
      return {...state, dishes: action.data};
    case ADD_TO_CART:
      let newOrder = {...state.order};
      if (state.order[action.id]) {
        newOrder[action.id]++
      } else {
        newOrder[action.id] = 1;
      }
      return {...state, order: newOrder, totalPrice: state.totalPrice + parseInt(action.price)};
    case REMOVE_FROM_CART:
      let orderCopy = {...state.order};
      orderCopy[action.id]--;
      return {...state, order: orderCopy, totalPrice: state.totalPrice - parseInt(action.price)};
    case CLEAR_TOTAL_PRICE:
      return {...state, totalPrice: 0};
    default:
      return state;
  }
};

export default reducer;