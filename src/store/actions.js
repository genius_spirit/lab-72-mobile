import axios from 'axios';

const url = 'https://blog-187e7.firebaseio.com';

export const FETCH_POST_REQUEST = "FETCH_POST_REQUEST";
export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_FAILURE = "FETCH_POST_FAILURE";
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CLEAR_TOTAL_PRICE = 'CLEAR_TOTAL_PRICE;'


export const getData = () => dispatch => {
    dispatch(fetchPostRequest());
    axios.get(`${url}/dishes.json`)
    .then(response => {
      dispatch(fetchPostSuccess(response.data));
    }, error => {
      dispatch(fetchPostFailure(error));
    });
};

export const sendData = (order) => (dispatch, getState) =>{
  // const order = getState().order;
  axios.post(`${url}/dishesOrder.json`, order);
};

export const addToCart = (id, price) => {
  return {type: ADD_TO_CART, id, price};
};

export const removeFromCart = (id, price) => {
  return {type: REMOVE_FROM_CART, id, price};
};

export const fetchPostRequest = () => {
  return {type: FETCH_POST_REQUEST};
};

export const fetchPostSuccess = (data) => {
  return {type: FETCH_POST_SUCCESS, data};
};

export const fetchPostFailure = (error) => {
  return {type: FETCH_POST_FAILURE, error};
};

export const clearTotalPrice = () => {
  return {type: CLEAR_TOTAL_PRICE};
};