import React from 'react';
import {Image, Text, TouchableNativeFeedback, View, StyleSheet} from "react-native";
const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    padding: 10,
    marginBottom: 10,
    width: '100%',
    borderWidth: 1,
    borderColor: '#777'
  },
  image: {
    width: 70,
    height: 70,
  },
  title: {
    fontSize: 25,
    color: '#333'
  },
  price: {
    marginRight: 10
  }
});

const DishItem = (props) => {
  return(
    <TouchableNativeFeedback onPress={props.clicked}>
      <View style={styles.item} >
        <Image resizeMode='contain' source={{uri: props.image}} style={styles.image} />
        <Text style={styles.title}>{props.title}</Text>
        <Text style={[styles.title, styles.price]}>{props.price} kgs</Text>
      </View>
    </TouchableNativeFeedback>
  )
};

export default DishItem;