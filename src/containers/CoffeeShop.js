import React, { Component } from 'react';
import {
  FlatList,
  View,
  StyleSheet,
  Text,
  Button,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native";
import {connect} from "react-redux";
import {addToCart, clearTotalPrice, getData, removeFromCart, sendData} from "../store/actions";
import DishItem from "../components/DishItem";

class CoffeeShop extends Component {

  state = {
    modalVisible: false
  };

  componentDidMount() {
    this.props.onGetData();
  }

  toggleModal = () => {
    this.setState(prevState => {
      return { modalVisible: !prevState.modalVisible}
    });
  };

  backToShopHandler = (order) => {
    this.props.sendData(order);
    this.toggleModal();
    this.props.clearTotalPrice();
  };

  render() {
    return (
      this.props.dishes ?
        <View style={styles.container}>
          <FlatList
            keyExtractor={(item, index) => index}
            style={styles.dishesList}
            data={Object.keys(this.props.dishes)}
            renderItem={({item}) => (
              <DishItem
                image={this.props.dishes[item].image}
                title={this.props.dishes[item].title}
                price={this.props.dishes[item].price}
                clicked={() => this.props.addToCart(item, this.props.dishes[item].price)}
              />
            )} />
          <View style={styles.totalOrder}>
            <View style={styles.orderTextView}>
              <Text style={styles.orderText}>Order total:</Text>
              <Text style={styles.orderText}>{this.props.totalPrice}</Text>
            </View>
            <Button title="Заказать" onPress={this.toggleModal}/>
          </View>
          <Modal
            animationType="slide"
            visible={this.state.modalVisible}
            onRequestClose={this.toggleModal}>
            <View style={styles.orderContainer}>
              <View>
                <Text style={styles.orderTitle}>Ваш заказ:</Text>
                <View style={styles.orderList}>
                  { Object.keys(this.props.order).map(item => {
                    return (
                      <View key={item} style={styles.orderItem}>
                        <Text style={styles.txt}>{this.props.dishes[item].title}</Text>
                        <Text style={styles.txt}>{this.props.order[item]}</Text>
                        <Text style={styles.txt}>{this.props.dishes[item].price} kgs</Text>
                        <TouchableWithoutFeedback onPress={() => this.props.removeFromCart(item, this.props.dishes[item].price)}><View style={styles.removeBtn}><Text style={styles.txt}>Remove</Text></View></TouchableWithoutFeedback>
                      </View> )
                  })}
                </View>
                <View style={styles.orderTotal}>
                  <Text style={styles.txtTotal}>Доставка: 150 kgs</Text>
                  <Text style={styles.txtTotal}>Сумма: {parseInt(this.props.totalPrice) + parseInt(150)} kgs</Text>
                </View>
              </View>
              <View style={styles.buttons}>
                <TouchableOpacity onPress={this.toggleModal}>
                  <View style={styles.btn}>
                    <Text style={styles.btnText}>Отмена</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.backToShopHandler(this.props.order)}>
                  <View style={styles.btn}>
                    <Text style={styles.btnText}>Заказать</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View> : null
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dishesList: {
    width: '100%',
    padding: 10
  },
  totalOrder: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: 1,
    padding: 8
  },
  orderTextView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '50%'
  },
  orderText: {
    fontSize: 25,
    color: '#333',
    marginBottom: 10
  },
  buttons: {
    width: '100%',
    height: 100
  },
  btn: {
    margin: 5,
    borderWidth: 1,
    borderColor: '#333',
    alignItems: 'center',
    backgroundColor: '#555'
  },
  btnText: {
    fontSize: 20,
    padding: 8,
    color: '#eee'
  },
  removeBtn: {
    borderWidth: 1,
    borderColor: '#333',
    borderRadius: 7
  },
  txt: {
    color: '#333',
    fontSize: 15,
    padding: 5
  },
  txtTotal: {
    color: '#333',
    fontSize: 20
  },
  orderContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  orderTitle: {
    fontSize: 20,
    color: '#333',
    textAlign: 'center'
  },
  orderList: {
    justifyContent: 'flex-start',
    width: '100%'
  },
  orderItem: {
    width: '80%',
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  orderTotal: {
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20
  },
});

mapStateToProps = state => {
  return {
    dishes: state.dishes,
    totalPrice: state.totalPrice,
    order: state.order
  }
};

mapDispatchToProps = dispatch => {
  return {
    onGetData: () => dispatch(getData()),
    addToCart: (id, price) => dispatch(addToCart(id, price)),
    removeFromCart: (id, price) => dispatch(removeFromCart(id, price)),
    sendData: (order) => dispatch(sendData(order)),
    clearTotalPrice: () => dispatch(clearTotalPrice())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CoffeeShop);